import * as fs from 'fs';

import { AudioFile } from 'audiocommons-core';
import { IAudioFormat } from 'audiocommons-core';
import { IAudioReadable } from 'audiocommons-core';

import { IAudioWavChunk } from '../interfaces/iaudio-wav-chunk';
import { IAudioWavFormatChunk } from '../interfaces/iaudio-wav-format-chunk';

import {
		GROUP_ID,
		FILE_LENGTH,
		RIFF_TYPE,

		CHUNK_SIZE,
		FORMAT_TAG,
		CHANNELS,
		SAMPLES_PER_SEC,
		AVERAGE_BYTES_PER_SEC,
		BLOCK_ALIGN
} from '../consts/audio-wav';

export class AudioWavFileSource extends AudioFile implements IAudioReadable {
	private static readChunkMetadata(fd: number): IAudioWavChunk {
		const data: Buffer = Buffer.alloc(GROUP_ID + CHUNK_SIZE);
		fs.readSync(fd, data, 0, data.length, null);
		
		const view: DataView = new DataView(data.buffer);

		const metadata: IAudioWavChunk = {
				groupId: data.toString().substr(0, 4),
				chunkSize: view.getInt32(GROUP_ID, true)
		};
		
		return metadata;
	}

	private static consume(fd: number, size: number): void {
		const buffer: Buffer = Buffer.alloc(size);
		fs.readSync(fd, buffer, 0, size, null);
	}

	private static readWavFormat(fd: number, metadata: IAudioWavChunk): IAudioWavFormatChunk {
		const data: Buffer = Buffer.alloc(metadata.chunkSize);
		fs.readSync(fd, data, 0, metadata.chunkSize, null);
		
		const view: DataView = new DataView(data.buffer);

		// tslint:disable:object-literal-sort-keys
		const wavFormat: IAudioWavFormatChunk = {
				groupId: metadata.groupId,
				chunkSize: metadata.chunkSize,
				formatTag: view.getUint16(0, true),
				channels: view.getUint16(FORMAT_TAG, true),
				samplesPerSec: view.getUint32(FORMAT_TAG + CHANNELS, true),
				avgBytesPerSec: view.getUint32(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC, true),
				blockAlign: view.getUint16(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC + AVERAGE_BYTES_PER_SEC, true),
				bitsPerSample: view.getUint16(FORMAT_TAG + CHANNELS + SAMPLES_PER_SEC + AVERAGE_BYTES_PER_SEC + BLOCK_ALIGN, true)
		};
		// tslint:enable:object-literal-sort-keys
		
		return wavFormat;
	}
		
	private static readHeaderAndAudioFormat(fd: number): IAudioFormat {
		// get the header out of the way
		AudioWavFileSource.consume(fd, GROUP_ID + FILE_LENGTH + RIFF_TYPE);
		
		const metadata: IAudioWavChunk = AudioWavFileSource.readChunkMetadata(fd);
		if (metadata.groupId !== 'fmt ') throw new Error('Unexpected data other than format chunk in WAV header');

		const wavFormat: IAudioWavFormatChunk = AudioWavFileSource.readWavFormat(fd, metadata);

		const audioFormat: IAudioFormat = {
				channels: wavFormat.channels,
				bitDepth: wavFormat.bitsPerSample,
				sampleRate: wavFormat.samplesPerSec
		};

		return audioFormat;
	}
	
	constructor(
			fd: number
	) {
		super(fd, AudioWavFileSource.readHeaderAndAudioFormat(fd));

		// skip any remaining non-data chunks
		while (true) {
			const metadata: IAudioWavChunk = AudioWavFileSource.readChunkMetadata(fd);
			if (metadata.groupId === 'data') break;
			
			AudioWavFileSource.consume(fd, metadata.chunkSize);
		}
	}

	read(dest: number[][], offset: number, length: number): Promise<number> {
		const bytes: number = length * this.bytesPerSample;
		const buffer: Buffer = Buffer.alloc(bytes);
		
		return new Promise((resolve: (read: number) => void, reject: (reason: any) => void): void => {
			fs.read(
					this.fd,
					buffer,
					0,
					bytes,
					null,
					(err: Error, _bytesRead: number, _buffer: Buffer): void => {
						if (err) {
							reject(err);
							return;
						}
						
						this.bufferToSamples(buffer, 0, bytes, dest, offset);
						resolve(length);
					}
			);
		});
	}
}
