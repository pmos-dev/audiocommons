// originally taken from MSDN url, but there are lots of mistakes in the article, so the numbers below are the right ones
// https://blogs.msdn.microsoft.com/dawate/2009/06/23/intro-to-audio-programming-part-2-demystifying-the-wav-format/

// tslint:disable:no-bitwise

export const GROUP_ID: number = 4;
export const FILE_LENGTH: number = 32 >> 3;
export const RIFF_TYPE: number = 4;

export const CHUNK_SIZE: number = 32 >> 3;
export const FORMAT_TAG: number = 16 >> 3;
export const CHANNELS: number = 16 >> 3;
export const SAMPLES_PER_SEC: number = 32 >> 3;
export const AVERAGE_BYTES_PER_SEC: number = 32 >> 3;
export const BLOCK_ALIGN: number = 16 >> 3;
export const BITS_PER_SAMPLE: number = 16 >> 3;
