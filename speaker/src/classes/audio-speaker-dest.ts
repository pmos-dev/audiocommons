// tslint:disable-next-line
const Speaker = require('speaker');

import { Audio } from 'audiocommons-core';
import { IAudioWritable } from 'audiocommons-core';
import { IAudioFormat } from 'audiocommons-core';

export class AudioSpeakerDest extends Audio implements IAudioWritable {
	private speaker: any;
	
	constructor(
			audioFormat: IAudioFormat
	) {
		super(audioFormat);
		
		this.speaker = new Speaker(audioFormat);
	}
	
	write(src: number[][], offset: number, length: number): Promise<void> {
		const bytes: number = length * this.bytesPerSample;
		const buffer: Buffer = Buffer.alloc(bytes);
		
		this.samplesToBuffer(src, offset, length, buffer, 0);
		
		return new Promise((resolve: () => void, _: (reason: any) => void): void => {
			this.speaker.write(
					buffer,
					(): void => {
						resolve();
					}
			);
		});
	}
}
