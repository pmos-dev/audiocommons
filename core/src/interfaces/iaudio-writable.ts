import { IAudioFormat } from './iaudio-format';

export interface IAudioWritable {
		getAudioFormat(): IAudioFormat;
		write(src: number[][], offset: number, length: number): Promise<void>;
}
