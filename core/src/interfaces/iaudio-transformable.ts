export interface IAudioTransformable {
		transform(sample: number[]): void;
}
