export interface IAudioFormat {
		channels: number;
		bitDepth: number;
		sampleRate: number;
}

export function isIAudioFormatsEqual(a: IAudioFormat, b: IAudioFormat): boolean {
	return a.channels === b.channels
			&& a.bitDepth === b.bitDepth
			&& a.sampleRate === b.sampleRate;
}
