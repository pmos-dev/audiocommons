import { IAudioFormat } from './iaudio-format';

export interface IAudioReadable {
		getAudioFormat(): IAudioFormat;
		read(dest: number[][], offset: number, length: number): Promise<number>;
}
