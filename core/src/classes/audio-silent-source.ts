import { IAudioReadable } from '../interfaces/iaudio-readable';
import { IAudioFormat } from '../interfaces/iaudio-format';

export class AudioSilentSource implements IAudioReadable {
	constructor(
			private readonly audioFormat: IAudioFormat
	) {}

	getAudioFormat(): IAudioFormat {
		return this.audioFormat;
	}
	
	async read(dest: number[][], offset: number, length: number): Promise<number> {
		for (let i = length; i-- > 0;) {
			for (let c = this.audioFormat.channels; c-- > 0;) {
				dest[offset + i][c] = 0;
			}
		}
		
		return length;
	}
}
