import { CommonsAsync } from 'tscommons-async';

import { IAudioReadable } from '../interfaces/iaudio-readable';
import { IAudioWritable } from '../interfaces/iaudio-writable';
import { isIAudioFormatsEqual } from '../interfaces/iaudio-format';

export class AudioPlayer {
	private injecteds: IAudioWritable[] = [];
	private paused: boolean = false;
	private abort: boolean = false;
	private active: boolean = false;
	
	constructor(
			private source: IAudioReadable,
			private dest: IAudioWritable
	) {
		if (!isIAudioFormatsEqual(this.source.getAudioFormat(), this.dest.getAudioFormat())) throw new Error('Source and destination audio formats do not match');
	}
	
	public addWritable(injected: IAudioWritable): void {
		this.injecteds.push(injected);
	}
	
	public removeWritable(injected: IAudioWritable): boolean {
		const index: number = this.injecteds.indexOf(injected);
		if (index === -1) return false;
		
		this.injecteds.splice(index, 1);
		
		return true;
	}
	
	public async preLoadDestination(ms: number): Promise<void> {
		const sampleBlockSize = Math.ceil(ms * (this.dest.getAudioFormat().sampleRate / 1000));
		
		const samples: number[][] = [];
		for (let i = sampleBlockSize; i-- > 0;) samples.push([ 0, 0 ]);
		
		await this.dest.write(samples, 0, sampleBlockSize);
	}
	
	public pause(): void {
		this.paused = true;
	}
	
	public resume(): void {
		this.paused = false;
	}
	
	public stop(): void {
		this.abort = true;
	}
	
	public isActive(): boolean {
		return this.active;
	}
	
	public async start(ms: number): Promise<void> {
		const sampleBlockSize = Math.ceil(ms * (this.dest.getAudioFormat().sampleRate / 1000));
		
		const samples: number[][] = [];
		for (let i = sampleBlockSize; i-- > 0;) samples.push([ 0, 0 ]);
		
		while (!this.abort) {
			this.active = true;
			
			await CommonsAsync.timeout(0);
			
			if (this.paused) continue;
			
			await this.source.read(samples, 0, sampleBlockSize);
			if (samples.length === 0) continue;
			
			for (const injected of this.injecteds) injected.write(samples, 0, sampleBlockSize);	// don't await
			
			await this.dest.write(samples, 0, sampleBlockSize);
		}
		
		this.active = false;
	}
}
