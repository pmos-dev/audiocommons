import { IAudioReadable } from '../interfaces/iaudio-readable';
import { IAudioFormat } from '../interfaces/iaudio-format';

export class AudioMultiplexor implements IAudioReadable {
	private readonly sources: IAudioReadable[] = [];
	
	constructor(
			private readonly audioFormat: IAudioFormat
	) {}

	getAudioFormat(): IAudioFormat {
		return this.audioFormat;
	}
	
	public addSource(source: IAudioReadable): void {
		this.sources.push(source);
	}
	
	public removeSource(source: IAudioReadable): boolean {
		const index: number = this.sources.indexOf(source);
		if (index === -1) return false;
		
		this.sources.splice(index, 1);
		
		return true;
	}
	
	async read(dest: number[][], offset: number, length: number): Promise<number> {
		const reads: number[][][] = this.sources
				.map((_source: IAudioReadable): number[][] => {
					return Array(length).fill(null).map((_: any): number[] => [ 0, 0 ]);
				});

		const promises: Promise<void>[] = this.sources
				.map(async (source: IAudioReadable, i: number): Promise<void> => {
					await source.read(reads[i], 0, length);
				});

		await Promise.all(promises);
		
		for (let i = length; i-- > 0;) {
			for (let c = this.audioFormat.channels; c-- > 0;) {
				let mix: number = 0;
				
				for (let s = this.sources.length; s-- > 0;) {
					mix += reads[s][i][c];
				}
				
				dest[offset + i][c] = mix;
			}
		}
		
		return length;
	}
}
