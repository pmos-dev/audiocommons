import { IAudioFormat } from '../interfaces/iaudio-format';

import { Audio } from './audio';

export abstract class AudioFile extends Audio {
	constructor(
			protected readonly fd: number,
			audioFormat: IAudioFormat
	) {
		super(audioFormat);
	}
}
