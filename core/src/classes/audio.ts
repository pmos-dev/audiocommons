import { IAudioFormat } from '../interfaces/iaudio-format';

type TBufferToSamples = (src: Buffer, srcOffset: number, length: number, dest: number[][], destOffset: number) => void;
type TSamplesToBuffer = (src: number[][], srcOffset: number, length: number, dest: Buffer, destOffset: number) => void;

const bufferToSamples1Channel16Bit: TBufferToSamples = (src: Buffer, srcOffset: number, length: number, dest: number[][], destOffset: number): void => {
	const view: DataView = new DataView(src.buffer);
	
	for (let s = length / 2; s-- > 0;) {
		dest[destOffset][0] = view.getInt16(srcOffset, true);
		
		srcOffset += 2;
		destOffset++;
	}
};

const samplesToBuffer1Channel16Bit: TSamplesToBuffer = (src: number[][], srcOffset: number, length: number, dest: Buffer, destOffset: number): void => {
	const view: DataView = new DataView(dest.buffer);
	
	for (let s = length; s-- > 0;) {
		let sample: number = src[srcOffset][0];

		// this is due to some crazy requirement of wav files to do with endianness and 2s compliment
		// see: https://blogs.msdn.microsoft.com/dawate/2009/06/23/intro-to-audio-programming-part-2-demystifying-the-wav-format/
		if (sample < -32760) sample = -32760;
		if (sample > 32760) sample = 32760;
		
		view.setInt16(destOffset, sample, true);
		
		srcOffset++;
		destOffset += 2;
	}
};

const bufferToSamples2Channel16Bit: TBufferToSamples = (src: Buffer, srcOffset: number, length: number, dest: number[][], destOffset: number): void => {
	const view: DataView = new DataView(src.buffer);
	
	for (let s = length / 4; s-- > 0;) {
		dest[destOffset][0] = view.getInt16(srcOffset, true);
		dest[destOffset][1] = view.getInt16(srcOffset + 2, true);
		
		srcOffset += 4;
		destOffset++;
	}
};

const samplesToBuffer2Channel16Bit: TSamplesToBuffer = (src: number[][], srcOffset: number, length: number, dest: Buffer, destOffset: number): void => {
	const view: DataView = new DataView(dest.buffer);
	
	for (let s = length; s-- > 0;) {
		let sample0: number = src[srcOffset][0];
		let sample1: number = src[srcOffset][1];

		// this is due to some crazy requirement of wav files to do with endianness and 2s compliment
		// see: https://blogs.msdn.microsoft.com/dawate/2009/06/23/intro-to-audio-programming-part-2-demystifying-the-wav-format/
		if (sample0 < -32760) sample0 = -32760;
		if (sample0 > 32760) sample0 = 32760;
		if (sample1 < -32760) sample1 = -32760;
		if (sample1 > 32760) sample1 = 32760;
		
		view.setInt16(destOffset, sample0, true);
		view.setInt16(destOffset + 2, sample1, true);
		
		srcOffset++;
		destOffset += 4;
	}
};

export abstract class Audio {
	protected bytesPerSample: number;
	
	private readonly bufferToSamplesFunction: TBufferToSamples;
	private readonly samplesToBufferFunction: TSamplesToBuffer;

	constructor(
			private readonly audioFormat: IAudioFormat
	) {
		switch (this.audioFormat.bitDepth) {
			case 16: {
				switch (this.audioFormat.channels) {
					case 1: {
						this.bufferToSamplesFunction = bufferToSamples1Channel16Bit;
						this.samplesToBufferFunction = samplesToBuffer1Channel16Bit;
						this.bytesPerSample = 2;
						break;
					}
					case 2: {
						this.bufferToSamplesFunction = bufferToSamples2Channel16Bit;
						this.samplesToBufferFunction = samplesToBuffer2Channel16Bit;
						this.bytesPerSample = 4;
						break;
					}
					default:
						throw new Error('Unsupported number of channels');
				}
				break;
			}
			default:
				throw new Error('Unsupported bit depth');
		}
	}
	
	public getAudioFormat(): IAudioFormat {
		return this.audioFormat;
	}

	protected bufferToSamples(src: Buffer, srcOffset: number, length: number, dest: number[][], destOffset: number): void {
		this.bufferToSamplesFunction(src, srcOffset, length, dest, destOffset);
	}

	protected samplesToBuffer(src: number[][], srcOffset: number, length: number, dest: Buffer, destOffset: number): void {
		this.samplesToBufferFunction(src, srcOffset, length, dest, destOffset);
	}
}
