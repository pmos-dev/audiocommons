import { AudioMultiplexor } from 'audiocommons-core';
import { IAudioFormat, isIAudioFormatsEqual } from 'audiocommons-core';
import { AudioFxVolume } from 'audiocommons-fx';

import { AudioTrack } from './audio-track';

export class AudioTrackMixer extends AudioTrack {
	private readonly multiplexor: AudioMultiplexor;

	private readonly tracks: Map<string, AudioTrack> = new Map<string, AudioTrack>();
	private readonly volumes: Map<string, AudioFxVolume> = new Map<string, AudioFxVolume>();
	
	constructor(
			audioFormat: IAudioFormat
	) {
		super(audioFormat);
	
		this.multiplexor = new AudioMultiplexor(audioFormat);
		this.loadSource(this.multiplexor);
	}
	
	public addTrack(name: string, track: AudioTrack): void {
		if (this.tracks.has(name)) throw new Error('Track already exists');

		const audioFormat: IAudioFormat = track.getAudioFormat();
		if (!isIAudioFormatsEqual(audioFormat, this.getAudioFormat())) throw new Error('Track audioFormat does not match');

		this.tracks.set(name, track);

		const volume: AudioFxVolume = new AudioFxVolume();
		volume.setEnabled(true);
		volume.setVolume(1.0);
		track.addFx(volume);

		this.volumes.set(name, volume);
		
		this.multiplexor.addSource(track);
	}

	public getTrack(name: string): AudioTrack|undefined {
		return this.tracks.get(name);
	}
	
	public removeTrack(name: string): boolean {
		const track: AudioTrack|undefined = this.getTrack(name);
		if (!track) return false;

		this.tracks.delete(name);
		
		this.multiplexor.removeSource(track);
		
		return true;
	}
	
	public setVolume(name: string, v: number): void {
		const volume: AudioFxVolume|undefined = this.volumes.get(name);
		if (!volume) throw new Error('No such track exists');
		
		volume.setVolume(v);
	}
}
