import { AudioSilentSource } from 'audiocommons-core';
import { IAudioFormat, isIAudioFormatsEqual } from 'audiocommons-core';
import { IAudioReadable } from 'audiocommons-core';
import { IAudioTransformable } from 'audiocommons-core';
import { AudioFxMute } from 'audiocommons-fx';
import { AudioFxVolume } from 'audiocommons-fx';
import { AudioFxPan } from 'audiocommons-fx';

export class AudioTrack implements IAudioReadable {
	private source: IAudioReadable;
	private readonly fxs: IAudioTransformable[] = [];
	private readonly mute: AudioFxMute;
	private readonly gain: AudioFxVolume;
	private readonly pan: AudioFxPan;
	
	constructor(
			private readonly audioFormat: IAudioFormat
	) {
		this.unloadSource();
		this.source = new AudioSilentSource(audioFormat);
	
		this.mute = new AudioFxMute();
		this.mute.setEnabled(false);
		this.addFx(this.mute);

		this.gain = new AudioFxVolume();
		this.gain.setEnabled(true);
		this.gain.setVolume(1.0);
		this.addFx(this.gain);

		this.pan = new AudioFxPan();
		this.pan.setEnabled(true);
		this.pan.setPan(0);
		this.addFx(this.pan);
	}
	
	public getAudioFormat(): IAudioFormat {
		return this.audioFormat;
	}
	
	public loadSource(source: IAudioReadable): boolean {
		const audioFormat: IAudioFormat = source.getAudioFormat();
		if (!isIAudioFormatsEqual(audioFormat, this.audioFormat)) return false;
		
		this.source = source;
		
		return true;
	}
	
	public unloadSource(): void {
		this.source = new AudioSilentSource(this.audioFormat);
	}
	
	public addFx(fx: IAudioTransformable): void {
		this.fxs.push(fx);
	}

	public setMute(state: boolean): void {
		this.mute.setEnabled(state);
	}
	
	public setGain(v: number): void {
		this.gain.setVolume(v);
	}
	
	public setPan(p: number): void {
		this.pan.setPan(p);
	}

	async read(dest: number[][], offset: number, length: number): Promise<number> {
		const result: number = await this.source.read(dest, offset, length);
		
		this.fxs
				.forEach((fx: IAudioTransformable): void => {
					for (let i = result, j = offset; i-- > 0; j++) {
						fx.transform(dest[j]);
					}
				});

		return result;
	}
}
