import { IAudioTransformable } from 'audiocommons-core';
import { IAudioFormat } from 'audiocommons-core';

export abstract class AudioFx implements IAudioTransformable {
	public static bpmSampleSize(bpm: number, audioFormat: IAudioFormat): number {
		return Math.floor((60 / bpm) * audioFormat.sampleRate);
	}
	
	enabled: boolean = false;
	
	setEnabled(enabled: boolean): void {
		this.enabled = enabled;
	}

	protected abstract apply(sample: number[]): void;
	
	transform(sample: number[]): void {
		if (!this.enabled) return;

		this.apply(sample);
	}
}
