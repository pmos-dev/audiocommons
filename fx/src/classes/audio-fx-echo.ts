import { IAudioFormat } from 'audiocommons-core';

import { AudioFx } from './audio-fx';

export class AudioFxEcho extends AudioFx {
	private history: number[][] = [];
	private offset: number = 0;
	
	constructor(
		private readonly length: number,
		private readonly depth: number = 0.6,
		bpm?: number,
		audioFormat?: IAudioFormat
	) {
		super();
		
		if (bpm) {
			if (!audioFormat) throw new Error('Length as bpm without audioFormat');
			this.length = AudioFx.bpmSampleSize(bpm, audioFormat) * length;
		}
		
		this.history = Array(this.length).fill(null);
		this.reset();
	}
	
	private reset(): void {
		this.history = this.history.map((_: any): number[] => [ 0, 0 ]);
		this.offset = 0;
	}
	
	setEnabled(enabled: boolean): void {
		super.setEnabled(enabled);
		this.reset();
	}
	
	apply(sample: number[]): void {
		const existing: number[] = this.history[this.offset];
		this.history[this.offset++] = [ sample[0], sample[1] ];
		
		if (this.offset >= this.length) this.offset = 0;
		
		sample[0] += existing[0] * this.depth;
		sample[1] += existing[1] * this.depth;
	}
}
