import { AudioFx } from './audio-fx';

export class AudioFxPan extends AudioFx {
	private ratioLeft: number = 1;
	private ratioRight: number = 1;
	
	public setPan(pan: number): void {
		this.ratioLeft = Math.max(0, Math.min(1, 1 - pan));
		this.ratioRight = Math.max(0, Math.min(1, 1 + pan));
	}
	
	apply(sample: number[]): void {
		// only supports two-channel
		if (sample.length !== 2) return;
		
		sample[0] *= this.ratioLeft;
		sample[1] *= this.ratioRight;
	}
}
