import { AudioFx } from './audio-fx';

export class AudioFxVolume extends AudioFx {
	private volume: number = 1;
	
	public setVolume(volume: number): void {
		this.volume = volume;
	}
	
	apply(sample: number[]): void {
		sample[0] *= this.volume;
		sample[1] *= this.volume;
	}
}
