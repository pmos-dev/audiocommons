import { AudioFx } from './audio-fx';

export class AudioFxMute extends AudioFx {
	apply(sample: number[]): void {
		sample[0] = 0;
		sample[1] = 0;
	}
}
